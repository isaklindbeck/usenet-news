#include "message_handler.h"
#include "server.h"
#include "connection.h"
#include "connectionclosedexception.h"
#include <dirent.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fstream>
#include <vector>
#include "exceptions.h"
#include <memory>
#include <iostream>
#include <fstream> 
#include <string>
#include <stdexcept>
#include <cstdlib>

struct Group {
	Group(int id, string name) : id(id), name(name) {};
	int id;
	string name;	
	bool operator<(const Group& g) const {return id < g.id;};
};

struct Article {
	Article(int id, string title, string author, string text) : id(id), title(title), author(author), text(text) {};
	int id;
	string title;
	string author;
	string text;
};

string getGroupName(int id) {
	string path = "./root/"+to_string(id);
	ifstream in(path);
	if(!in)
		throw GroupNotFoundException();
	string name;
	getline(in, name);
	in.close();
	return name;
}

vector<Group> getGroups() {
	vector<Group> ret;
	auto d = opendir("root");
	auto f = readdir(d);
	while(f != NULL) {
		if(f->d_type == DT_REG) {
			int id = stoi(f->d_name);
			string file(f->d_name);
			string path = "./root/"+file;
			ifstream in(path);
			string name;
			getline(in, name);
			ret.push_back(Group(id, name));
			in.close();
		}
		f = readdir(d); 
	}
	closedir(d);
	return ret;
}

void createGroup(string name) {
	string path = "./root/"+name;
	int res = mkdir(path.c_str(), S_IRWXU);
	if(res != 0)
		throw GroupAlreadyExistsException();
	ifstream in("groupCount");
	string line;
	getline(in, line);
	in.close();
	int id = stoi(line);
	
	path = "./root/" + to_string(++id);
	ofstream out(path);
	out << name;
	out.close();
	
	ofstream count("groupCount");
	count << to_string(id);
	count.close();
}
void deleteGroup(int id) {
	string path = "./root/"+to_string(id);
	ifstream in(path);
	if(!in)
		throw GroupNotFoundException();
	string name = getGroupName(id);
	remove(path.c_str());
	path = "./root/" + name;
	remove(path.c_str());
}

vector<Article> getArticles(int groupid) {
	vector<Article> ret;
	string name = getGroupName(groupid);
	string path = "./root/"+name;
	auto d = opendir(path.c_str());
	auto f = readdir(d);
	while(f != NULL) {
		if(f->d_type == DT_REG) {
			int id = stoi(f->d_name);
			string file(f->d_name);
			string path = "./root/"+name+"/"+file;
			ifstream in(path);
			string title;
			string author;
			string text;
			getline(in, title);
			getline(in, author);
			getline(in, text);
			string line;
			while(getline(in, line)) {
				text += "\n" + line;
			}
			ret.push_back(Article(id, title, author, text));
			in.close();
		}
		f = readdir(d); 
	}

	return ret;
}

Article getArticle(int groupid, int articleid) {
	string name = getGroupName(groupid);
	string path = "./root/" + name + "/" + to_string(articleid);
	ifstream in(path);
	if(!in)
		throw ArticleNotFoundException();
	string title;
	string author;
	string text;
	getline(in, title);
	getline(in, author);
	getline(in, text);
	string line;
	while(getline(in, line)) {
		text += "\n" + line;
	}
	return Article(articleid, title, author, text);
}

void createArticle(int groupid, string title, string author, string text) {
	string name = getGroupName(groupid);
	
	ifstream in("articleCount");
	string line;
	getline(in, line);
	in.close();
	int id = stoi(line);
	
	string path = "./root/" + name + "/" + to_string(++id);
	ofstream out(path);
	out << title << endl;
	out << author<< endl;
	out << text << endl;
	out.close();
	
	ofstream count("articleCount");
	count << to_string(id);
	count.close();
}

void deleteArticle(int groupid, int articleid) {
	string name = getGroupName(groupid);
	string path = "./root/" + name + "/" + to_string(articleid);
	int res = remove(path.c_str());
	if(res != 0)
		throw ArticleNotFoundException();
}

int main(int argc, char* argv[]){

	string path = "./root";
	mkdir(path.c_str(), S_IRWXU);
	ifstream gc("groupCount");
	if(!gc) {
		ofstream out("groupCount");
		out << 0;
		out.close();
	}
	gc.close();

	ifstream ac("articleCount");
	if(!ac) {
		ofstream out("articleCount");
		out << 0;
		out.close();
	}
	ac.close();

	if (argc != 2) {
		cerr << "Usage: on_disk_server port-number" << endl;
		exit(1);
	}
	
	int port = -1;
	try {
		port = stoi(argv[1]);
	} catch (exception& e) {
		cerr << "Wrong port number. " << e.what() << endl;
		exit(1);
	}
	
	Server server(port);
	if (!server.isReady()) {
		cerr << "Server initialization error." << endl;
		exit(1);
	}
	
	while(true) {
		auto conn = server.waitForActivity();
		if (conn != nullptr) {
			try {
				unsigned char cmd = readByte(conn);
				switch(cmd) {
				case Protocol::COM_LIST_NG:
				{
					expectByte(conn, Protocol::COM_END);
					auto v = getGroups();
					writeByte(conn, Protocol::ANS_LIST_NG);
					write_num_p(conn, v.size());
					for (auto g : v) {
						write_num_p(conn, g.id);
						write_string_p(conn, g.name);
					}
					writeByte(conn, Protocol::ANS_END);
					break;
				}
				case Protocol::COM_CREATE_NG:
				{
					string name = read_string_p(conn);
					expectByte(conn, Protocol::COM_END);
					writeByte(conn, Protocol::ANS_CREATE_NG);
					try {
						createGroup(name);
						writeByte(conn, Protocol::ANS_ACK);
					} catch (GroupAlreadyExistsException&) {
						writeByte(conn, Protocol::ANS_NAK);
						writeByte(conn, Protocol::ERR_NG_ALREADY_EXISTS);
					}
					writeByte(conn, Protocol::ANS_END);
					break;
				}
				case Protocol::COM_DELETE_NG:
				{
					int id = read_num_p(conn);
					expectByte(conn, Protocol::COM_END);
					writeByte(conn, Protocol::ANS_DELETE_NG);
					try {
						deleteGroup(id);
						writeByte(conn, Protocol::ANS_ACK);
					} catch (GroupNotFoundException&) {
						writeByte(conn, Protocol::ANS_NAK);
						writeByte(conn, Protocol::ERR_NG_DOES_NOT_EXIST);
					}
					writeByte(conn, Protocol::ANS_END);
					break;
				}
				case Protocol::COM_LIST_ART:
				{
					int id = read_num_p(conn);
					expectByte(conn, Protocol::COM_END);
					writeByte(conn, Protocol::ANS_LIST_ART);
					try {
						auto v = getArticles(id);
						writeByte(conn, Protocol::ANS_ACK);
						write_num_p(conn, v.size());
						for (auto a : v) {
							write_num_p(conn, a.id);
							write_string_p(conn, a.title);
						}
					} catch (GroupNotFoundException&) {
						writeByte(conn, Protocol::ANS_NAK);
						writeByte(conn, Protocol::ERR_NG_DOES_NOT_EXIST);
					}
					writeByte(conn, Protocol::ANS_END);
					break;
				}
				case Protocol::COM_CREATE_ART:
				{
					int id = read_num_p(conn);
					string title = read_string_p(conn);
					string author = read_string_p(conn);
					string text = read_string_p(conn);
					expectByte(conn, Protocol::COM_END);
					writeByte(conn, Protocol::ANS_CREATE_ART);
					try {
						createArticle(id, title, author, text);
						writeByte(conn, Protocol::ANS_ACK);
					} catch (GroupNotFoundException&) {
						writeByte(conn, Protocol::ANS_NAK);
						writeByte(conn, Protocol::ERR_NG_DOES_NOT_EXIST);
					}
					writeByte(conn, Protocol::ANS_END);
					break;
				}
				case Protocol::COM_DELETE_ART:
				{
					int gid = read_num_p(conn);
					int aid = read_num_p(conn);
					expectByte(conn, Protocol::COM_END);
					writeByte(conn, Protocol::ANS_DELETE_ART);
					try{
						deleteArticle(gid, aid);
						writeByte(conn, Protocol::ANS_ACK);
					} catch (GroupNotFoundException&) {
						writeByte(conn, Protocol::ANS_NAK);
						writeByte(conn, Protocol::ERR_NG_DOES_NOT_EXIST);
					} catch (ArticleNotFoundException&) {
						writeByte(conn, Protocol::ANS_NAK);
						writeByte(conn, Protocol::ERR_ART_DOES_NOT_EXIST);
					}
					writeByte(conn, Protocol::ANS_END);
					break;
				} 
				case Protocol::COM_GET_ART:
				{
					int gid = read_num_p(conn);
					int aid = read_num_p(conn);
					expectByte(conn, Protocol::COM_END);
					writeByte(conn, Protocol::ANS_GET_ART);
					try{
						Article a = getArticle(gid, aid);
						writeByte(conn, Protocol::ANS_ACK);
						write_string_p(conn, a.title);
						write_string_p(conn, a.author);
						write_string_p(conn, a.text);
					} catch (GroupNotFoundException&) {
						writeByte(conn, Protocol::ANS_NAK);
						writeByte(conn, Protocol::ERR_NG_DOES_NOT_EXIST);
					} catch (ArticleNotFoundException&) {
						writeByte(conn, Protocol::ANS_NAK);
						writeByte(conn, Protocol::ERR_ART_DOES_NOT_EXIST);
					}
					writeByte(conn, Protocol::ANS_END);
					break;
				}
				default:
					throw ConnectionFormatException();
				}
			} catch (ConnectionClosedException&) {
				server.deregisterConnection(conn);
				cout << "Client closed connection" << endl;
			} catch (ConnectionFormatException&) {
				server.deregisterConnection(conn);
				cout << "Received bad data, client disconected." << endl;
			}
		} else {
			conn = make_shared<Connection>();
			server.registerConnection(conn);
			cout << "New client connects" << endl;
		}
	}
	
}
