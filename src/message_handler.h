#ifndef MSGHANDLER_H
#define MSGHANDLER_H

#include "connection.h"
#include "protocol.h"
#include "exceptions.h"
#include <string>
#include <memory>
#include <stdexcept>
#include <cstdlib>

using namespace std;

int readNumber(const shared_ptr<Connection>& conn) {
	unsigned char byte1 = conn->read();
	unsigned char byte2 = conn->read();
	unsigned char byte3 = conn->read();
	unsigned char byte4 = conn->read();
	return (byte1 << 24) | (byte2 << 16) | (byte3 << 8) | byte4;
}

void writeNumber(const shared_ptr<Connection>& conn, int value) {
	conn->write((value >> 24) & 0xFF);
	conn->write((value >> 16) & 0xFF);
	conn->write((value >> 8)	 & 0xFF);
	conn->write(value & 0xFF);
}

unsigned char readByte(const shared_ptr<Connection>& conn) {
	unsigned char byte= conn->read();
	return byte;
}

void writeByte(const shared_ptr<Connection>& conn, unsigned char byte) {
	conn->write(byte);
}

void expectByte(const shared_ptr<Connection>& conn, unsigned char ebyte) {
	unsigned char rbyte = conn->read();
	if(ebyte != rbyte)
		throw ConnectionFormatException();
}

int read_num_p(const shared_ptr<Connection>& conn) {
	expectByte(conn, Protocol::PAR_NUM);
	return readNumber(conn);
}

void write_num_p(const shared_ptr<Connection>& conn, int n) {
	writeByte(conn, Protocol::PAR_NUM);
	writeNumber(conn, n);
}

string read_string_p(const shared_ptr<Connection>& conn) {
	expectByte(conn, Protocol::PAR_STRING);
	int n = readNumber(conn);
	string ret;
	char c;
	for(int i = 0; i < n; ++i) {
		c = conn->read();
		ret += c; 
	}
	return ret;
}

void write_string_p(const shared_ptr<Connection>& conn, const string& s) {
	writeByte(conn, Protocol::PAR_STRING);
	writeNumber(conn, s.length());
	for (char c : s) {
		conn->write(c);
	}
}

#endif
