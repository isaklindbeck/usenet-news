#include "message_handler.h"
#include "server.h"
#include "connection.h"
#include "connectionclosedexception.h"

#include <utility>
#include <algorithm>
#include <vector>
#include	<map>
#include "exceptions.h"
#include <memory>
#include <iostream>
#include <string>
#include <stdexcept>
#include <cstdlib>

using namespace std;

struct Group {
	Group(int id, string name) : id(id), name(name) {};
	int id;
	string name;
	
	bool operator<(const Group& g) const {return id < g.id;};
};

struct Article {
	Article(int id, string title, string author, string text) : id(id), title(title), author(author), text(text) {};
	int id;
	string title;
	string author;
	string text;
};

map<Group, vector<Article>> groups;
int groupCount = 0;
int articleCount = 0;

vector<Group> getGroups() {
	vector<Group> ret;
	for(auto p : groups) {
		ret.push_back(p.first);
	}
	return ret;
}

void createGroup(string name) {
	auto it = find_if(groups.begin(), groups.end(), [name](pair<Group, vector<Article>> p){return p.first.name == name;});
	if(it != groups.end())
		throw GroupAlreadyExistsException();
	groups[Group(++groupCount, name)];
}

void deleteGroup(int groupid) {
	auto it = find_if(groups.begin(), groups.end(), [groupid](pair<Group, vector<Article>> p){return p.first.id == groupid;});
	if(it == groups.end())
		throw GroupNotFoundException();
	groups.erase(it);
}

vector<Article>& getArticles(int groupid) {
	auto it = find_if(groups.begin(), groups.end(), [groupid](pair<Group, vector<Article>> p){return p.first.id == groupid;});
	if(it == groups.end())
		throw GroupNotFoundException();
	return it->second;
}

Article getArticle(int groupid, int articleid) {
	vector<Article> v = getArticles(groupid);
	for(auto a : v) {
		if(a.id == articleid)
			return a;
	}
	throw ArticleNotFoundException();
}

void createArticle(int groupid, string title, string author, string text) {
	vector<Article>& v = getArticles(groupid);
	Article a(++articleCount, title, author, text);
	v.push_back(a);	
}

void deleteArticle(int groupid, int articleid) {
	vector<Article>& v = getArticles(groupid);
	auto it = find_if(v.begin(), v.end(), [articleid](Article a){return a.id == articleid;});
	if(it == v.end())
		throw ArticleNotFoundException();
	v.erase(it);
}


int main(int argc, char* argv[]){
	if (argc != 2) {
		cerr << "Usage: in_memory_server port-number" << endl;
		exit(1);
	}
	
	int port = -1;
	try {
		port = stoi(argv[1]);
	} catch (exception& e) {
		cerr << "Wrong port number. " << e.what() << endl;
		exit(1);
	}
	
	Server server(port);
	if (!server.isReady()) {
		cerr << "Server initialization error." << endl;
		exit(1);
	}
	
	while(true) {
		auto conn = server.waitForActivity();
		if (conn != nullptr) {
			try {
				unsigned char cmd = readByte(conn);
				switch(cmd) {
				case Protocol::COM_LIST_NG:
				{
					expectByte(conn, Protocol::COM_END);
					auto v = getGroups();
					writeByte(conn, Protocol::ANS_LIST_NG);
					write_num_p(conn, v.size());
					for (auto g : v) {
						write_num_p(conn, g.id);
						write_string_p(conn, g.name);
					}
					writeByte(conn, Protocol::ANS_END);
					break;
				}
				case Protocol::COM_CREATE_NG:
				{
					string name = read_string_p(conn);
					expectByte(conn, Protocol::COM_END);
					writeByte(conn, Protocol::ANS_CREATE_NG);
					try {
						createGroup(name);
						writeByte(conn, Protocol::ANS_ACK);
					} catch (GroupAlreadyExistsException&) {
						writeByte(conn, Protocol::ANS_NAK);
						writeByte(conn, Protocol::ERR_NG_ALREADY_EXISTS);
					}
					writeByte(conn, Protocol::ANS_END);
					break;
				}
				case Protocol::COM_DELETE_NG:
				{
					int id = read_num_p(conn);
					expectByte(conn, Protocol::COM_END);
					writeByte(conn, Protocol::ANS_DELETE_NG);
					try {
						deleteGroup(id);
						writeByte(conn, Protocol::ANS_ACK);
					} catch (GroupNotFoundException&) {
						writeByte(conn, Protocol::ANS_NAK);
						writeByte(conn, Protocol::ERR_NG_DOES_NOT_EXIST);
					}
					writeByte(conn, Protocol::ANS_END);
					break;
				}
				case Protocol::COM_LIST_ART:
				{
					int id = read_num_p(conn);
					expectByte(conn, Protocol::COM_END);
					writeByte(conn, Protocol::ANS_LIST_ART);
					try {
						auto v = getArticles(id);
						writeByte(conn, Protocol::ANS_ACK);
						write_num_p(conn, v.size());
						for (auto a : v) {
							write_num_p(conn, a.id);
							write_string_p(conn, a.title);
						}
					} catch (GroupNotFoundException&) {
						writeByte(conn, Protocol::ANS_NAK);
						writeByte(conn, Protocol::ERR_NG_DOES_NOT_EXIST);
					}
					writeByte(conn, Protocol::ANS_END);
					break;
				}
				case Protocol::COM_CREATE_ART:
				{
					int id = read_num_p(conn);
					string title = read_string_p(conn);
					string author = read_string_p(conn);
					string text = read_string_p(conn);
					expectByte(conn, Protocol::COM_END);
					writeByte(conn, Protocol::ANS_CREATE_ART);
					try {
						createArticle(id, title, author, text);
						writeByte(conn, Protocol::ANS_ACK);
					} catch (GroupNotFoundException&) {
						writeByte(conn, Protocol::ANS_NAK);
						writeByte(conn, Protocol::ERR_NG_DOES_NOT_EXIST);
					}
					writeByte(conn, Protocol::ANS_END);
					break;
				}
				case Protocol::COM_DELETE_ART:
				{
					int gid = read_num_p(conn);
					int aid = read_num_p(conn);
					expectByte(conn, Protocol::COM_END);
					writeByte(conn, Protocol::ANS_DELETE_ART);
					try{
						deleteArticle(gid, aid);
						writeByte(conn, Protocol::ANS_ACK);
					} catch (GroupNotFoundException&) {
						writeByte(conn, Protocol::ANS_NAK);
						writeByte(conn, Protocol::ERR_NG_DOES_NOT_EXIST);
					} catch (ArticleNotFoundException&) {
						writeByte(conn, Protocol::ANS_NAK);
						writeByte(conn, Protocol::ERR_ART_DOES_NOT_EXIST);
					}
					writeByte(conn, Protocol::ANS_END);
					break;
				} 
				case Protocol::COM_GET_ART:
				{
					int gid = read_num_p(conn);
					int aid = read_num_p(conn);
					expectByte(conn, Protocol::COM_END);
					writeByte(conn, Protocol::ANS_GET_ART);
					try{
						Article a = getArticle(gid, aid);
						writeByte(conn, Protocol::ANS_ACK);
						write_string_p(conn, a.title);
						write_string_p(conn, a.author);
						write_string_p(conn, a.text);
					} catch (GroupNotFoundException&) {
						writeByte(conn, Protocol::ANS_NAK);
						writeByte(conn, Protocol::ERR_NG_DOES_NOT_EXIST);
					} catch (ArticleNotFoundException&) {
						writeByte(conn, Protocol::ANS_NAK);
						writeByte(conn, Protocol::ERR_ART_DOES_NOT_EXIST);
					}
					writeByte(conn, Protocol::ANS_END);
					break;
				}
				default:
					throw ConnectionFormatException();
				}
			} catch (ConnectionClosedException&) {
				server.deregisterConnection(conn);
				cout << "Client closed connection" << endl;
			} catch (ConnectionFormatException&) {
				server.deregisterConnection(conn);
				cout << "Received bad data, client disconected." << endl;
			}
		} else {
			conn = make_shared<Connection>();
			server.registerConnection(conn);
			cout << "New client connects" << endl;
		}
	}
	
}
