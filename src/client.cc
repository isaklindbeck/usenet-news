#include "message_handler.h"
#include "connection.h"
#include "connectionclosedexception.h"
#include <sstream>
#include <utility>
#include <algorithm>
#include <vector>
#include	<map>
#include "exceptions.h"
#include <memory>
#include <iostream>
#include <string>
#include <stdexcept>
#include <cstdlib>

using namespace std;

enum command {
	DEFAULT,
	COM_LIST_NG,
	COM_CREATE_NG,
	COM_DELETE_NG,
	COM_LIST_ART,
	COM_CREATE_ART,
	COM_DELETE_ART,
	COM_GET_ART,
	HELP,
	EXIT
};

string helptxt = "\tlistgroups\n\tcreategroup [groupname]\n\tdeletegroup [groupid]\n\tlistarticles [groupid]\n\tcreatearticle [title] [author] [text*]\n\tdeletearticle [groupid] [articleid]\n\tgetarticle [groupid] [articleid]\n\thelp\n\texit";

void initMap(map<string, command>& m) {
	m["listgroups"] 		= COM_LIST_NG;
	m["creategroup"] 		= COM_CREATE_NG;
	m["deletegroup"] 		= COM_DELETE_NG;
	m["listarticles"] 	= COM_LIST_ART;
	m["createarticle"] 	= COM_CREATE_ART;
	m["deletearticle"] 	= COM_DELETE_ART;
	m["getarticle"] 		= COM_GET_ART;
	m["help"] 				= HELP;
	m["exit"]				= EXIT;
}

int getint(istringstream& iss) {
	if(iss.rdbuf()->in_avail() == 0)
		throw BadInputException();
	string s;
	iss >> s;
	int ret;
	try {
		ret = stoi(s);
	} catch (invalid_argument&) {
		throw BadInputException();
	}
	return ret;
}

string getstring(istringstream& iss) {
	if(iss.rdbuf()->in_avail() == 0)
		throw BadInputException();
	string s;
	iss >> s;
	return s;
}

int main(int argc, char* argv[]){
	if (argc != 3) {
		cerr << "Usage: myclient host-name port-number" << endl;
		exit(1);
	}
	
	int port = -1;
	try {
		port = stoi(argv[2]);
	} catch (exception& e) {
		cerr << "Wrong port number. " << e.what() << endl;
		exit(1);
	}
	
	shared_ptr<Connection> conn(new Connection(argv[1], port));
	if (!conn->isConnected()) {
		cerr << "Connection attempt failed" << endl;
		exit(1);
	}
	map<string, command> m;
	initMap(m);
	
	cout << "Type help for available commands." << endl;
	
	string input;
	while(cin) {
		getline(cin, input);
		istringstream iss(input);
		string word = getstring(iss);
		try {
		switch(m[word]) {
			case COM_LIST_NG:
			{
				writeByte(conn, Protocol::COM_LIST_NG);
				writeByte(conn, Protocol::COM_END);
				
				expectByte(conn, Protocol::ANS_LIST_NG);
				int n = read_num_p(conn);
				for(int i = 0; i < n; ++i) {
					int id = read_num_p(conn);
					string name = read_string_p(conn);
					cout << "\t" << id << ": " << name << endl;
				}
				expectByte(conn, Protocol::ANS_END);
				break;
			}
			case COM_CREATE_NG:
			{
				string name;
				iss >> name;
				writeByte(conn, Protocol::COM_CREATE_NG);
				write_string_p(conn, name);
				writeByte(conn, Protocol::COM_END);
				
				expectByte(conn, Protocol::ANS_CREATE_NG);
				auto byte = readByte(conn);
				if(byte == Protocol::ANS_ACK) {
					cout << "Group sucessfuly created." << endl;
				} else if(byte == Protocol::ANS_NAK) {
					expectByte(conn, Protocol::ERR_NG_ALREADY_EXISTS);
				} else {
					throw ConnectionFormatException();
				}
				expectByte(conn, Protocol::ANS_END);
				break;
			}
			case COM_DELETE_NG:
			{
				int id = getint(iss);
				writeByte(conn, Protocol::COM_DELETE_NG);
				write_num_p(conn, id);
				writeByte(conn, Protocol::COM_END);
				
				expectByte(conn, Protocol::ANS_DELETE_NG);
				auto byte = readByte(conn);
				if(byte == Protocol::ANS_ACK) {
					cout << "Group sucessfuly deleted." << endl;
				} else if(byte == Protocol::ANS_NAK) {
					expectByte(conn, Protocol::ERR_NG_DOES_NOT_EXIST);
				} else {
					throw ConnectionFormatException();
				}
				expectByte(conn, Protocol::ANS_END);
				break;
			}
			case COM_LIST_ART:
			{
				int id = getint(iss);
				writeByte(conn, Protocol::COM_LIST_ART);
				write_num_p(conn, id);
				writeByte(conn, Protocol::COM_END);
				expectByte(conn, Protocol::ANS_LIST_ART);
				auto byte = readByte(conn);
				if(byte == Protocol::ANS_ACK) {
					int n = read_num_p(conn);
					for(int i = 0; i < n; ++i) {
						id = read_num_p(conn);
						string title = read_string_p(conn);
						cout << "\t" << id << ": " << title << endl;
					}
				} else if(byte == Protocol::ANS_NAK) {
					expectByte(conn, Protocol::ERR_NG_DOES_NOT_EXIST);
				} else {
					throw ConnectionFormatException();
				}
				expectByte(conn, Protocol::ANS_END);
				break;
			}
			case COM_CREATE_ART:
			{
				int id = getint(iss);
				string title = getstring(iss);
				string author = getstring(iss);
				string text;
				iss.get();
				getline(iss, text);
				writeByte(conn, Protocol::COM_CREATE_ART);
				write_num_p(conn, id);
				write_string_p(conn, title);
				write_string_p(conn, author);
				write_string_p(conn, text);
				writeByte(conn, Protocol::COM_END);
				expectByte(conn, Protocol::ANS_CREATE_ART);
				auto byte = readByte(conn);
				if(byte == Protocol::ANS_ACK) {
					cout << "Article sucessfuly created." << endl;
				} else if(byte == Protocol::ANS_NAK) {
					expectByte(conn, Protocol::ERR_NG_DOES_NOT_EXIST);
				} else {
					throw ConnectionFormatException();
				}
				expectByte(conn, Protocol::ANS_END);
				break;
			}
			case COM_DELETE_ART:
			{
				int id1 = getint(iss);
				int id2 = getint(iss);
				writeByte(conn, Protocol::COM_DELETE_ART);
				write_num_p(conn, id1);
				write_num_p(conn, id2);
				writeByte(conn, Protocol::COM_END);
				expectByte(conn, Protocol::ANS_DELETE_ART);
				auto byte = readByte(conn);
				if(byte == Protocol::ANS_ACK) {
					cout << "Article sucessfuly deleted." << endl;
				} else if(byte == Protocol::ANS_NAK) {
					byte = readByte(conn);
					if(byte == Protocol::ERR_NG_DOES_NOT_EXIST) {
						cout << "Group not found." << endl;
					} else if(byte == Protocol::ERR_ART_DOES_NOT_EXIST) {
						cout << "Article not found." << endl;
					} else {
						throw ConnectionFormatException();
					}
				} else {
					throw ConnectionFormatException();
				}
				expectByte(conn, Protocol::ANS_END);
				break;
			}
			case COM_GET_ART:
			{
				int id1 = getint(iss);
				int id2 = getint(iss);
				writeByte(conn, Protocol::COM_GET_ART);
				write_num_p(conn, id1);
				write_num_p(conn, id2);
				writeByte(conn, Protocol::COM_END);
				expectByte(conn, Protocol::ANS_GET_ART);
				auto byte = readByte(conn);
				if(byte == Protocol::ANS_ACK) {
					string title = read_string_p(conn);
					string author = read_string_p(conn);
					string text = read_string_p(conn);
					cout << "Title: " << title << endl;
					cout << "Author: " << author << endl;
					cout << text << endl;
				} else if(byte == Protocol::ANS_NAK) {
					byte = readByte(conn);
					if(byte == Protocol::ERR_NG_DOES_NOT_EXIST) {
						cout << "Group not found." << endl;
					} else if(byte == Protocol::ERR_ART_DOES_NOT_EXIST) {
						cout << "Article not found." << endl;
					} else {
						throw ConnectionFormatException();
					}
				} else {
					throw ConnectionFormatException();
				}
				expectByte(conn, Protocol::ANS_END);
				break;
			}
			case HELP:
			{
				cout << helptxt << endl;
				break;
			}
			case EXIT:
			{
				exit(0);
				break;
			}
			default:
				throw BadInputException();
		}
		} catch(ConnectionFormatException&) {
			cout << "Received bad data." << endl;
		} catch(BadInputException&) {
			cout << "Unrecognized command, type help for available commands."<<endl;
		}
		cin.clear();
	}
}






