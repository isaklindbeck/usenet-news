#ifndef EXCEPTION_H
#define EXCEPTION_H

struct ConnectionFormatException {};
struct GroupNotFoundException {};
struct GroupAlreadyExistsException{};
struct ArticleNotFoundException {};
struct BadInputException {};

#endif
